Contributing to Flectra
====================
#Part of code based on https://github.com/OCA/pylint-odoo

#TODO

[Full contribution guidelines](https://github.com/flectrahq/flectra/wiki/Contributing)

TL;DR

* If you [make a pull request](https://github.com/flectrahq/flectra/wiki/Contributing#making-pull-requests),
  do not create an issue! Use the PR description for that
* Issues are handled with a much lower priority than pull requests
CONTRIBUTING.md
